DOCKER_IMG_NAME := devops-challenge
DOCKER_IMG_TAG  := 1.0.0

run_docker_cmd := docker run \
		--rm \
		-it \
		-p 5000:5000 \
		-e FLASK_DATABASE_USER -e FLASK_DATABASE_PASSWORD -e FLASK_DATABASE_HOST \
		-e FLASK_DATABASE_PORT -e FLASK_DATABASE_NAME \
		$(DOCKER_IMG_NAME):$(DOCKER_IMG_TAG)

docker-run-gunicorn: docker-build
	docker run \

docker-build:
	docker build -t $(DOCKER_IMG_NAME):$(DOCKER_IMG_TAG) .

docker-run: docker-build
	$(run_docker_cmd)

docker-run-gunicorn: docker-build
	$(run_docker_cmd) \
			gunicorn --bind 0.0.0.0:5000 --forwarded-allow-ips='*' wsgi:app
