# * Use official Python container with Alpine builds for decreased app container size.
#    
#    Do not pin to container hash, so that we can continue to get updates of the base
#    container automatically (& pray it doesn't break upstream).

FROM python:3.10-alpine

WORKDIR /app

# * Upgrade any existing apk packages.
# * Copy the dependencies and install them.
#    
#    A multistage build won't necessarily help here. We'd need to track down all
#    files created by the pip and poetry builds and copy them into the production
#    image - which isn't impossible, but adds a burden of tracking them down
#    when/if dependencies change.
# 
# * Pin versions of poetry and gunicorn below.

COPY alembic.ini poetry.lock pyproject.toml /app/
RUN apk upgrade -U --no-cache && \
    apk add --no-cache --virtual .build-deps \
        build-base \
        gcc \
        musl-dev \
        libffi-dev \
        openssl-dev \
        postgresql-dev \
        && \
    pip --no-cache-dir install \
        poetry==1.1.13 \
        gunicorn==20.1.0 \
        && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction --no-ansi && \
    apk del .build-deps && \
    apk add -U --no-cache libpq

# * After the dependencies are built/installed, copy in any changed code.
#   Preserves earlier build cache.

COPY app /app/app
COPY migrations /app/migrations

# * Copy an entrypoint to run db migrations and run flask with gunicorn.

COPY docker-entrypoint.sh wsgi.py ./
CMD ["./docker-entrypoint.sh"]
