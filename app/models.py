import sqlalchemy as sa

from sqlalchemy.orm import declarative_base, relationship


BaseModel = declarative_base()


class Author(BaseModel):
    __tablename__ = 'Author'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, nullable=False)
    quotes = relationship('Quote', back_populates='author')


class Quote(BaseModel):
    __tablename__ = 'Quote'

    id = sa.Column(sa.Integer, primary_key=True)
    quote = sa.Column(sa.Text, nullable=False)
    author_id = sa.Column(sa.Integer, sa.ForeignKey('Author.id'), nullable=False)
    author = relationship('Author', back_populates='quotes')
