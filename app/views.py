from flask import Blueprint, render_template

from app import models
from app.extensions import db


bp = Blueprint('app', __name__, template_folder='templates', static_folder='static')


@bp.route('/')
def index():
    return render_template('index.html', quotes=db.query(models.Quote).all())
