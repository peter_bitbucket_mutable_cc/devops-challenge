
# New Readme

## Requirements
 - `make`
 - Docker
 - A Postgres database accessible from a Docker container

## Setup
1. Check out this repository.
2. Change to the directory of this repository.
3. Export the following environment variables to point to a Postgres database:

   | Environment Variable Name     | Description                                                           |
   |-------------------------------|-----------------------------------------------------------------------|
   | `FLASK_DATABASE_NAME`         | The logical database name                                             |
   | `FLASK_DATABASE_USER`         | The name of the database user                                         |
   | `FLASK_DATABASE_PASSWORD`     | The name of the database password                                     |
   | `FLASK_DATABASE_HOST`         | The host or IP of the database (accessible from the Docker container) |
   | `FLASK_DATABASE_PORT`         | The port of the database                                              |

## Quick Start

### Build the app container
```bash
$ make docker-build
```

### Run the app container (only gunicorn, no flask upgrade)
```bash
$ make docker-run-gunicorn
```

### Run the app container
```bash
$ make docker-run
```

### Test the app container
```bash
$ curl localhost:5000
```


---

# eIQ DevOps Take-Home

There's a small [Flask](https://flask.palletsprojects.com/en/2.0.x/) app here, which uses the [application factory pattern](https://flask.palletsprojects.com/en/2.0.x/patterns/appfactories/) and includes a [SQLAlchemy](https://www.sqlalchemy.org/) database configured to use PostgreSQL in production. It uses [Alembic](https://alembic.sqlalchemy.org/en/latest/) migrations to create the tables in the database and populate them with some sample data. There's also some static files which must be served, in the `app/static` directory. Dependencies are managed using [Poetry](https://python-poetry.org/docs/).

In development, the app can be run like so (using SQLite):

```bash
cd devops-challenge
poetry install
flask db upgrade
FLASK_DEBUG="true" flask run
```

Environment variables you should be aware of include:

1. `FLASK_ENV`: Should be set to `"prod"`.
2. `FLASK_DATABASE_USER`, `FLASK_DATABASE_PASSWORD`, `FLASK_DATABASE_HOST`, `FLASK_DATABASE_PORT`, and `FLASK_DATABASE_NAME`. 

Your mission is to serve this app in production on [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) using Infrastructure-as-Code and best practices.

Some pointers:

1. You will want to containerize the app: [Docker](https://www.docker.com/) and [gunicorn](https://gunicorn.org/) are popular choices.
2. You should use something like [Terraform](https://www.terraform.io/) to provision and deploy the app to a cluster.
3. The data in the database should persist independent of the application's lifecycle.

Please include a `README` explaining the steps necessary to deploy the app, as well as providing some reasoning around the implementation choices you made. Your deliverable should also include a link to the running app, the deployment code itself, and for bonus points, it would also be awesome to include some pointers around scaling and fault tolerance considerations, although they need not be implemented.
